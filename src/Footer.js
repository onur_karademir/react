import React, { Component } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";

export default class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hadoken: [
        { id: 0, name: "ken" },
        { id: 1, name: "ryu" },
      ],
      dinamikVeri:""
    };
  }
  dinamikFonksiyon = (parametre)=>{
    this.setState({dinamikVeri:parametre.name})
  }

  render() {
    return (
      <div>
        <h4>{this.props.info.title}</h4>
        <p>{this.props.info.name}</p>
        <ListGroup>
         {this.state.hadoken.map((parametre)=>(
          <ListGroupItem onClick={()=>this.dinamikFonksiyon(parametre)} key={parametre.id}>{parametre.name}</ListGroupItem>
         ))}
        </ListGroup>

         <strong>{this.state.dinamikVeri}</strong>
      </div>
    );
  }
}
