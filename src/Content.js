import React, { Component} from 'react';
import logo from './img/alt.jpg';

export default class Content extends Component {
    render() {
        return (
            <div className="bg-success">
            <div className="container p-5">
                <div className="row">
                    <div className="col-md-6 order-md-1">
                        <p>Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500'lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960'larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.</p>
                    </div>
    
                    <div className="col-md-6 order-md-2">
                        <img src={logo} alt={"logo"} className="img-thumbnail img-fluid"></img>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
