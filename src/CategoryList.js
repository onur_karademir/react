import React, { Component } from "react";
import { ListGroup, ListGroupItem } from "reactstrap";

export default class Catagory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        { id: 0, isim: "onur" },
        { id: 1, isim: "yunus" },
        { id: 2, isim: "hatice" },
      ],
    };
  }
  

  render() {
    return (
      <div>
        <h2>{this.props.info.title}</h2>
        <ListGroup>
          {this.state.data.map(myData =>(
          <ListGroupItem onClick={()=>this.props.mydynamic(myData)} key={myData.id}>{myData.isim}</ListGroupItem>
          ))}
        </ListGroup>
        <h3>{this.props.catagoryDinamic}</h3>
      </div>
    );
  }
}
