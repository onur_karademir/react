import React, { Component } from "react";
import Navi from "./Navi";
import CategoryList from "./CategoryList";
import ProductList from "./ProductList";
import Footer from "./Footer";
import { Container, Row, Col } from "reactstrap";
import Content from "./Content";

export default class App extends Component {
  state={catagoryDinamic:""}
  mydynamic=(myData)=>{
    this.setState({catagoryDinamic:myData.isim})
  }
  render() {
    let categoryList = { title: "catagori başığı" };
    let productList = { title: "product başlığı" };
    let footerArea = { title: "footer başlığı", name: "Onur Karademir" };
    return (
      <div>
        <Navi></Navi>
        <Container>
            
          <Row>
            <Col xs="3">
              <CategoryList catagoryDinamic={this.state.catagoryDinamic} mydynamic={this.mydynamic} info={categoryList}></CategoryList>
            </Col>
            <Col xs="9">
              <ProductList info={productList}></ProductList>
            </Col>
          </Row>
          <Row>
            <Footer info={footerArea}></Footer>
          </Row>
          <Content></Content>
        </Container>
      </div>
    );
  }
}
