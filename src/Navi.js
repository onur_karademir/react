import React, { Component } from 'react';
import { Navbar } from 'reactstrap';

export default class Navi extends Component {
    render() {
        return (
            <Navbar className="navbar-nav bg-dark navbar-dark navbar-expand-md">
                <div className="container">
                <button className="navbar-toggler" data-toggle='collapse' data-target='#col-nav'>
                    <span className="navbar-toggler-icon"></span> 
                </button>
                <div className="navbar-brand">
                    <h4>Build By React Tech</h4>
                </div>
                <div className="collapse navbar-collapse" id="col-nav">
                    <ul className="navbar-nav" id="nav" style={{margin: "0 auto"}}>
                    </ul>

                    <form action="" className="form-inline">
                        <input type="text" className="form-control mr-1" placeholder="Arama Yap">
                            </input>
                        <button type="button" className="btn btn-outline-primary">ara</button>
                    </form>

                </div> 
                </div>
            </Navbar>
        )
    }
}
